# Take Home Assignment for Haystack

Hello to anyone who will review this. I'm Arturo Cuya and this is my submission for the Take Home Assignment for Haystack. In this README I will go through my understanding of the problem, how I planned to solve it, my pain points and things that I would improve in a second iteration of this assignment.

# The problem

In summary, this is a simple app that allows the user to navigate through a grid of thumbnail images queried from Flickr. If the user clicks on an image, a fullscreen version of it should appear in a new page. The main screen should also let the user search images by text and load them in a new page.

Some implementation caveats are:
1. The thumbnails must be lazy loaded as the user scrolls down through the grid
2. The app must not run out of memory
3. The full screen images must be of a higher resolution than the thumbnail ones

# How I planned to solve it

Based on a past experience that I had with Android, the first thing that came to mind when thinking about lazy loading images and not running out of memory was to use a RecyclerView for the grid. This view manages the efficient scrolling of items, loading them when necessary and caching the ones that are not in the screen at any moment. I found that for Android Tv there is a similar implementation called GridView that extends RecyclerView.

For the actual rendering of the bitmaps I also needed a tool that could do that efficiently. For that I found in the Android Tv documentation a library called Glide which is a "fast and efficient open source media management and image loading framework for Android that wraps media decoding, memory and disk caching, and resource pooling". I found Glide easy to use since it provides a simple function to render bitmaps based on a URL.

To show a high resolution image when a thumbnail is clicked, I found that Flickr differentiates image sizes with a suffix at the end of the URL. For example:

* https://live.staticflickr.com/65535/50222514456_a449e510ec_m.jpg - with the _m at the end describes a regular sized image. I used this suffix for the thumbnails

* https://live.staticflickr.com/65535/50222514456_a449e510ec_b.jpg -  with the _b at the end shows the same image in a much higher resolution. I used this suffix for the fullscreen pictures.

To get the images info (name, author, date and url) I used a public feed that Flickr provides which can be filtered by tags:
https://www.flickr.com/services/feeds/docs/photos_public/


# Painpoints

## Android (Tv) is a new environment for me

I have worked with Android in the past in small projects and in a university course. I learned enough about Java and the Android DevEnv to make simple applications but never went deep enough into the best practices. I wanted to apply the concept of Components to this project like in React but I couldn't understand in time how to use Fragments for this. That's why in the ThumbnailRowAdapter the main logic is unnecesarily reused.

I have never worked before with Android Tv so in the beginning of the solving process I spent quite some time configuring the emulator so that it wouldn't eat my 16GB of RAM. I couldn't grasp completely how to direct the navigation inside a grid of elements. I understood that it works automatically with the orientation of a LinearLayout and the left/rightTo constraints set in a RelativeLayout. But I didn't understand how to manage the navigation flow for elements with different parents. For that reason the navigation in the grid is functional enough to be used but can be improved.

For the same reason that is described in the first paragraph I could not implement the carousel for when the app is showing a fullscreen image. To work around the fact that I was not using Fragments for thumbnails in the same row I had to divide the initial set of images into groups of three. After establishing that overly complicacted structure, I couldn't figure out in time how to apply it to a carousel.

## The Flickr API and feed were problematic
I had trouble using the actual Flickr API. After generating the API key needed for the search feature the API kept saying that my token was incorrect. Frustrated, I sticked to using the public feed for search too, applying the "tags" param before firing the request. There were two problems with the public feed:

* It only responds with a set of 20 images. It's enough to demonstrate the lazy loading but obviously the results end too quickly.
* It doesn't have any kind of NSFW filter, so I saw weird stuff when testing it

UPDATE: In the last couple of hours before presenting my solution, I figured out how to use the API. I didn't do anything differently from the first time. It appears that my key needed some time to be enabled. Anyways, the response format is way different from the public feed response, so I couldn't make any changes in time.

# Conclusion

I think this was a great assignment to test how someone new to the team would respond to a completely new tech stack in a short period of time. Which is precisely the kind of experience I would have the first months working in Haystack. I think my solution is functional enough to be an MVP, but I could do much better with a proper mentor in the subject. I could have done more since you didn't specify a time limit, but I wanted to constraint myself to max 3 days. Otherwise I would have worked on this endlessly which I don't think is the point for this assignment.