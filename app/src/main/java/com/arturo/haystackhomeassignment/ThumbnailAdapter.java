package com.arturo.haystackhomeassignment;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.leanback.widget.HorizontalGridView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class ThumbnailAdapter extends HorizontalGridView.Adapter<ThumbnailAdapter.ViewHolder> {
    public ArrayList<ArrayList<ImageModel>> images;
    private Context ctx;
    public ImageModel currentImage;


    public ThumbnailAdapter(ArrayList<ArrayList<ImageModel>> images, Context ctx) {
        this.images = images;
        this.ctx = ctx;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView firstThumbnailLineOne, firstThumbnailLineTwo;
        RelativeLayout firstThumbnailContainer;
        ImageView firstThumbnailImage;

        TextView secondThumbnailLineOne, secondThumbnailLineTwo;
        RelativeLayout secondThumbnailContainer;
        ImageView secondThumbnailImage;

        TextView thirdThumbnailLineOne, thirdThumbnailLineTwo;
        RelativeLayout thirdThumbnailContainer;
        ImageView thirdThumbnailImage;

        GradientDrawable border = new GradientDrawable();
        GradientDrawable noBorder = new GradientDrawable();

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            firstThumbnailLineOne = itemView.findViewById(R.id.thumbnail_one_name);
            firstThumbnailLineTwo = itemView.findViewById(R.id.thumbnail_one_author);
            firstThumbnailContainer = itemView.findViewById(R.id.thumbnail_one_container);
            firstThumbnailImage = itemView.findViewById(R.id.thumbnail_one_image);

            secondThumbnailLineOne = itemView.findViewById(R.id.thumbnail_two_name);
            secondThumbnailLineTwo = itemView.findViewById(R.id.thumbnail_two_author);
            secondThumbnailContainer = itemView.findViewById(R.id.thumbnail_two_container);
            secondThumbnailImage = itemView.findViewById(R.id.thumbnail_two_image);

            thirdThumbnailLineOne = itemView.findViewById(R.id.thumbnail_three_name);
            thirdThumbnailLineTwo = itemView.findViewById(R.id.thumbnail_three_author);
            thirdThumbnailContainer = itemView.findViewById(R.id.thumbnail_three_container);
            thirdThumbnailImage = itemView.findViewById(R.id.thumbnail_three_image);

            border.setColor(0xff000000);
            border.setStroke(5, 0xffffffff);
            noBorder.setColor(0xff000000);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.fragment_thumbnail_row, parent, false);

        return new ViewHolder(view);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (currentImage.uri.length() > 0) {
                Intent intent = new Intent(ctx, BigPictureActivity.class);
                intent.putExtra("uri", currentImage.uri);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(intent);
            }
        }
    };

    private View.OnFocusChangeListener onFocusChangeListener (final RelativeLayout container,
                                                              final GradientDrawable bor,
                                                              final GradientDrawable noBor,
                                                              final ImageModel imgToSet) {
        return new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b) {
                    container.setBackground(bor);
                    currentImage = imgToSet;
                } else {
                    container.setBackground(noBor);
                }
            }
        };
    }

    @Override
    public void onBindViewHolder(@NonNull final ThumbnailAdapter.ViewHolder holder, int position) {
        final ArrayList<ImageModel> imageList = images.get(position);
        ImageModel emptyImage = new ImageModel("","","","");

        final ImageModel firstImage, secondImage, thirdImage;

        firstImage = imageList.get(0);
        secondImage = imageList.get(1);
        thirdImage = imageList.get(2);

        holder.firstThumbnailLineOne.setText(firstImage.name);
        holder.firstThumbnailLineTwo.setText(String.format("%s / %s", firstImage.author, firstImage.date));

        holder.secondThumbnailLineOne.setText(secondImage.name);
        holder.secondThumbnailLineTwo.setText(String.format("%s / %s", secondImage.author, secondImage.date));

        holder.thirdThumbnailLineOne.setText(thirdImage.name);
        holder.thirdThumbnailLineTwo.setText(String.format("%s / %s", thirdImage.author, thirdImage.date));

        Glide.with(ctx)
                .load(firstImage.uri)
                .fitCenter()
                .into(holder.firstThumbnailImage);

        Glide.with(ctx)
                .load(secondImage.uri)
                .fitCenter()
                .into(holder.secondThumbnailImage);

        Glide.with(ctx)
                .load(thirdImage.uri)
                .fitCenter()
                .into(holder.thirdThumbnailImage);

        holder.firstThumbnailContainer.setOnClickListener(onClickListener);
        holder.secondThumbnailContainer.setOnClickListener(onClickListener);
        holder.thirdThumbnailContainer.setOnClickListener(onClickListener);

        holder.firstThumbnailContainer
                .setOnFocusChangeListener(onFocusChangeListener(
                        holder.firstThumbnailContainer,
                        holder.border,
                        holder.noBorder,
                        firstImage
                ));

        holder.secondThumbnailContainer
                .setOnFocusChangeListener(onFocusChangeListener(
                        holder.secondThumbnailContainer,
                        holder.border,
                        holder.noBorder,
                        secondImage
                ));

        holder.thirdThumbnailContainer
                .setOnFocusChangeListener(onFocusChangeListener(
                        holder.thirdThumbnailContainer,
                        holder.border,
                        holder.noBorder,
                        thirdImage
                ));

    }

    @Override
    public int getItemCount() {
        return images.size();
    }
}
