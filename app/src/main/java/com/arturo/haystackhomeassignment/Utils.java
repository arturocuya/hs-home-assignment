package com.arturo.haystackhomeassignment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

public final class Utils {
    public static String parseAuthorName(String rawName) {
        boolean grab = false;
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < rawName.length(); i++) {
            if (rawName.charAt(i) == '"') {
                grab = !grab;
                i++;
            }
            if (grab) {
                result.append(rawName.charAt(i));
            }
        }

        return String.valueOf(result);
    }

    public static String parseTitle(String rawTitle) {
        if (!Pattern.matches(".*[a-zA-Z].*", rawTitle) && !rawTitle.equals("")) {
            return "Untitled";
        }

        return rawTitle;
    }

    public static String parseDate(String rawDate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);

        try {
            Date date = format.parse(rawDate);

            SimpleDateFormat myFormat = new SimpleDateFormat("MMM dd yyyy", Locale.US);
            return myFormat.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "No date";
    }

}
