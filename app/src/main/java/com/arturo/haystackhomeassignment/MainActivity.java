package com.arturo.haystackhomeassignment;

import androidx.leanback.widget.HorizontalGridView;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends Activity {
    HorizontalGridView horizontalGridView;
    HorizontalGridView.Adapter thumbnailAdapter;
    RequestQueue queue;
    String endpoint = "https://www.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1";

    GradientDrawable searchBorder = new GradientDrawable();
    GradientDrawable searchNoBorder = new GradientDrawable();

    RelativeLayout searchContainer;
    EditText searchInput;
    TextView mainTitle;

    String searchTerm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        searchBorder.setColor(0xff37959F);
        searchBorder.setCornerRadius(200);

        searchNoBorder.setColor(0xff37959F);
        searchNoBorder.setCornerRadius(200);

        searchBorder.setStroke(5, 0xffffffff);

        searchContainer = this.findViewById(R.id.search_container);
        searchContainer.setBackground(searchNoBorder);

        mainTitle = this.findViewById(R.id.main_title);

        searchInput = this.findViewById(R.id.search_input);

        searchTerm = getIntent().getStringExtra("SEARCH_TERM");



        searchContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Hide search button and show input
                searchContainer.setVisibility(View.INVISIBLE);
                searchInput.setVisibility(View.VISIBLE);
                searchInput.requestFocus();

                // Show keyboard
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.showSoftInput(searchInput, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        searchContainer.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    searchContainer.setBackground(searchBorder);
                } else {
                    searchContainer.setBackground(searchNoBorder);
                }
            }
        });

        searchInput.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If "Enter" is pressed
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {

                    // Hide input and show search button
                    System.out.println(searchInput.getText().toString());
                    searchContainer.requestFocus();
                    searchContainer.setVisibility(View.VISIBLE);
                    searchInput.setVisibility(View.INVISIBLE);

                    // Hide keyboard
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(searchInput.getWindowToken(), 0);

                    String term = searchInput.getText().toString();
                    searchInput.getText().clear();

                    if (searchTerm == null) {


                        // Search in new activity
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.putExtra("SEARCH_TERM", term);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        getApplicationContext().startActivity(intent);

                    } else {
                        // Search in same activity
                        loadImages(term);
                    }

                    return true;
                }
                return false;
            }
        });

        horizontalGridView = findViewById(R.id.hgv);
        horizontalGridView.setLayoutManager(new LinearLayoutManager(this));
        queue = Volley.newRequestQueue(this);

        if (searchTerm != null) {
            loadImages(searchTerm);
        } else loadImages("");


    }

    // Divide images in rows of three
    protected void insertImages(ArrayList<ImageModel> images) {
        ArrayList<ArrayList<ImageModel>> result = new ArrayList<>();

        // Fill until there are 3n groups
        int subdivisions = (int)Math.floor((double)images.size() / 3);
        ImageModel emptyImage = new ImageModel("", "", "", "");

        int toAdd = (subdivisions + 1) * 3 - images.size();
        for (int i = 0; i < toAdd; i++) {
            images.add(emptyImage);
        }

        // Divide images
        ArrayList<ImageModel> element = new ArrayList<>();
        for (int i = 0; i < images.size(); i++) {
            element.add(images.get(i));

            if ((i > 0 && (i + 1) % 3 == 0) || i == images.size() - 1) {
                result.add(element);
                element = new ArrayList<>();
            }
        }

        // Populate the adapter with sectioned images
        thumbnailAdapter = new ThumbnailAdapter(result, this.getApplicationContext());
        horizontalGridView.setAdapter(thumbnailAdapter);
    }

    void loadImages (String searchTerm) {

        if (searchTerm.equals("")) {
            mainTitle.setText("Trending on Flickr");
        } else {
            mainTitle.setText(String.format("Search results for \"%s\"", searchTerm));
        }

        searchInput.getText().clear();

        String uri = endpoint;
        if (!searchTerm.equals("")) {
            uri += "&tags=" + searchTerm;
        }

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, uri, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray items = (JSONArray) response.get("items");

                    ArrayList<ImageModel> images = new ArrayList<>();

                    for (int i = 0; i < items.length(); ++i) {
                        JSONObject jsonImage = items.getJSONObject(i);
                        ImageModel img = new ImageModel(
                                Utils.parseTitle(jsonImage.get("title").toString()),
                                Utils.parseAuthorName(jsonImage.get("author").toString()),
                                Utils.parseDate(jsonImage.get("date_taken").toString()),
                                ((JSONObject)jsonImage.get("media")).get("m").toString());

                        images.add(img);
                    }


                    insertImages(images);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mainTitle.setText("No results");
                Toast.makeText(getApplicationContext(), "Error while fetching data. Please check your internet connection", Toast.LENGTH_LONG).show();
            }
        });

        req.setRetryPolicy(new DefaultRetryPolicy(5*DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        queue.add(req);
    }

}