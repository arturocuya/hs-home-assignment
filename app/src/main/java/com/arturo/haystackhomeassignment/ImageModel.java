package com.arturo.haystackhomeassignment;

public class ImageModel {
    String name, author, date, uri;

    public ImageModel(String name, String author, String date, String uri) {
        this.name = name;
        this.author = author;
        this.date = date;
        this.uri = uri;
    }
}
