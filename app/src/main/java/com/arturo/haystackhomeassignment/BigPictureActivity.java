package com.arturo.haystackhomeassignment;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class BigPictureActivity extends Activity {
    String uri;
    ImageView bigPicture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_big_picture);

        uri = getIntent().getStringExtra("uri");
        bigPicture = this.findViewById(R.id.big_picture);

        Glide.with(this)
                .load(uri.replace("_m.jpg","_b.jpg"))
                .fitCenter()
                .into(bigPicture);
    }
}